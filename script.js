const readline = require('readline');
const math = require('mathjs');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// rl.on('line', (line) => {
//   let nums = line.split(' ');
//   let a = parseFloat(nums[0]);
//   let b = parseFloat(nums[1]);
//   let c = parseFloat(nums[2]);
//   let d = parseFloat(nums[3]);
//   let p = parseFloat(nums[4]);

//   for (i = 0; i < line.length; i++) {
//     res = ((math.norm([c, d], p)) - (math.norm([a, b], p)))
//   }

//   console.log(res);
// });

// THIS IS THE CODE THAT PRODUCES THE CORRECT OUTPUT WHEN RUNNING 'node script.js' - BUT IT HAS TO LIVE IN rl.on('line', (line) =>{}) ABOVE
let array = [1.0, 1.0, 20.0, 20.0, 10.0];

for (i = 0; i < array.length; i++) {
  res = ((math.norm([array[2], array[3]], array[4])) - (math.norm([array[0], array[1]], array[4])))
}

console.log(res);
